@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Employee</div>

                <div class="form-group">
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th scope="col">Firstname</th>
                        <th scope="col">Lastname</th>
                        <th scope="col">Birthday</th>
                        <th scope="col">Emai</th>
                        <th  scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Hattie</td>
                        <td>Schamberger</td>
                        <td>1987-11-22</td>
                        <td>Caol41@yahoo.com</td>
                        <td>
                            <button class="btn btn-primary">Update</button>
                            <button class="btn btn-danger">Delete</button>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
